import { Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'FlickerCalculator';

  onhitPercent = 20;
  onhitIgCrit = 0;

  enemies = 1;
  enemiesHit = 1;
  multistrikeUsage = "s";

  percentForOneOrMore;
  frenzyExpected;

  probOne;

  percentForNone;
  percentForOne;

  ngOnInit(){
    this.caculateFrenzy();
  }

  caculateFrenzy() {
    console.log(this.multistrikeUsage)

    var onhitDecimal = this.onhitPercent/100;
    var onhitIgCritDecimal = this.onhitIgCrit/100;
  
    this.probOne = 1-((1-onhitDecimal)*(1-onhitIgCritDecimal));

    if(this.multistrikeUsage == "m"){
      this.enemiesHit = this.enemies * 3;
    }
    else if(this.multistrikeUsage == "mPlus"){
      this.enemiesHit = this.enemies * 4;
    }
    else {
      this.enemiesHit = this.enemies;
    }

    this.percentForNone = (this.calculateBinomial(0,this.enemiesHit,this.probOne)*100).toFixed(2);
    this.percentForOne = (this.calculateBinomial(1,this.enemiesHit,this.probOne)*100).toFixed(2);
    this.percentForOneOrMore = (100 - this.percentForNone).toFixed(2);
    
    this.frenzyExpected = (this.recurseFrenzyExpected(this.enemiesHit, 0)).toFixed(2);

  }

  recurseFrenzyExpected(attempts, aggregator){
    if(attempts <= 0){
      return aggregator;
    }
    else {
      return this.recurseFrenzyExpected(attempts-1, aggregator + attempts * this.calculateBinomial(attempts, this.enemiesHit,this.probOne));
    }
  }

  calculateBinomial(equals,attempts,probability) {
    var binomial = (
      this.factorial(attempts)
        /
      (this.factorial(equals) * this.factorial(attempts-equals))
    ) * (probability**(equals)) * ((1-probability)**(attempts-equals))
    return binomial;
  }

  factorial(n: number) {
    if(n <= 1){
      return 1;
    }
    else{
      return n * this.factorial(n-1);
    }
  }
}
