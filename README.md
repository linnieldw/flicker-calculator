# FlickerCalculator

This project was meant to calculate the chance of getting a frenzy charge on a flicker use given a % chance of gaining a frenzy on hit/ignite/crit.
To use it, simply fill in the % chance and it'll do the rest.
With assumptions:
 - Use of multistrike (3 hits)
 - 100% accuracy
 - 1 enemy hit
 - Starting from 0 Frenzy charges

## Why?

I see many people not calculating the % chance properly. 20% chance and 3 hits from multistrike? That must mean 60% chance! OMG!
Unfortunately, each individual event is discrete, which means this is can be modeled through discrete binomial distribution.

I've made the source code avalable for anyone to review, fork or whatever.
If you spot any mistakes, drop me a message or merge request and I'll try to get back to you.